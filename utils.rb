# frozen_string_literal: true

require 'json'

def get_user_data(user_id)
  data = get_parsed_data_from_file('./input.json')
  rewards = data['rewards']
  loyalty_cards = data['loyalty_cards']
  cards = []
  total_points = 0

  rewards.each do |reward|
    next unless reward['user_id'] == user_id

    card_name = get_card_name(loyalty_cards, reward['loyalty_card_id'])
    card = Hash[
      'id' => reward['loyalty_card_id'],
      'points' => reward['points'],
      'name' => card_name
    ]
    old_card = cards.select { |c| c['id'] == card['id'] }
    cards.delete_if { |c| c['id'] == card['id'] }

    if cards.length.positive? && old_card.length.positive?
      points = old_card[0]['points'] + reward['points']
      card['points'] = points
    end

    cards << card
    total_points += reward['points']
  end

  {
    id: user_id,
    total_points: total_points,
    loyalty_cards: cards
  }
end

def get_card_data(card_id)
  data = get_parsed_data_from_file('./input.json')
  rewards = data['rewards']
  loyalty_cards = data['loyalty_cards']
  total_points = 0
  card_name = get_card_name(loyalty_cards, card_id)

  rewards.each do |reward|
    next unless reward['loyalty_card_id'] == card_id

    total_points += reward['points']
  end

  {
    id: card_id,
    name: card_name,
    total_points: total_points
  }
end

def get_card_name(cards, card_id)
  card = cards.select { |c| c['id'] == card_id }
  card[0]['name']
end

def get_parsed_data_from_file(file)
  file = File.read(file)
  JSON.parse(file)
end
