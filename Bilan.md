# Bilan du FidMe Ruby challenge

### Retour d'expérience
Pour quelqu'un qui n'avait pas apprécié ses cours de Ruby on Rails... j'ai plutôt apprécié faire cet exercice :).

J'ai perdu pas mal de temps sur :
- les erreurs de conversions String / Integer ;
- les règles de style de code (qui sont visiblement très nombreuses...) ;
- les recherches que j'ai dû effectuer du fait que je ne connaissais pas le langage.

### Points d'améliorations
- Le fichier utils.rb est très général, dans une application de plus grande ampleur j'aurais certainement redivisé les 
fonctions dans des fichiers supplémentaires.
- Il y a quelques indices 0 pour faire appel au premier résultat dans le tableau, ce n'est pas très propre, mais je sais 
qu'ici il n'y aura qu'un seul résultat. J'aurai pu faire appel à la fonction .first ou .last.


### Temps
2 h 30 - 3 h 00


### Bonne lecture !
Mathieu Brung