# frozen_string_literal: true

require 'optparse'
require 'json'
require './utils'

options = {}
result = {}

OptionParser.new do |opt|
  opt.on('--user_id USER_ID') { |o| options[:user_id] = o }
  opt.on('--loyalty_card_id LOYALTY_CARD_ID') { |o| options[:loyalty_card_id] = o }
end.parse!

user_id_value = Integer(options[:user_id])
loyalty_card_id_value = Integer(options[:loyalty_card_id])

result[:user] = get_user_data(user_id_value) if options[:user_id]
result[:loyalty_card] = get_card_data(loyalty_card_id_value) if options[:loyalty_card_id]

puts JSON.pretty_generate(result)
